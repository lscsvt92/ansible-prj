#!/usr/bin/python
# -*- coding: utf-8 -*-
#License: Public Domain
ANSIBLE_METADATA = {'metadata_version': '1.1',
                    'status': ['stableinterface'],
                    'supported_by': 'core'}
DOCUMENTATION = r'''
---
module: hyperv-checkpoint
short_description: ansible module to create vm checkpoints in Hyper-V
description:
  - create checkpoint
  - remove a single checkpoint or all checkpoins of a vm
  - set a name or a description of an existing vm checkpoint
options:
  scvmm:
    description:
      - FQDN of Hyper-V VMM
    mandatory: yes
  username:
    description:
      - username to access in Hyper-V VMM
    mandatory: yes
  password:
    description:
      - userneame's password
    mandatory: yes
  vm:
    description:
      - vm name to perform checkpoint taak
    mandatory: yes
  state:
    description:
      - describe which chackpoint task must be perfom on the vm
      - create: create a vm checkpoint
      - remove: remove a single vm checkpoint
      - remove_all: remove all vm checkpoints
      - set: set a name or description of an existing vm checkpoint
    mandatory: yes
  checkpoint_name:
    description:
      - it is the name of vm checkpoint. It is required in 'remove' and 'set' state options.
    mandatory: no
  checkpoint_descr:
    description:
      - it is a description of checkpoint. It is not mandatory
    mandatory: no
  checkpoint_new_name:
    description:
      - it is a new name setting in a existing vm checkpoint. it can use with 'set' state option
    mandatory: no
  checkpoint_new_descr:
    description:
      - it ia a new decription setting a existing vm checkpont. it can use with 'set' state option
    mandatory: no

author:
- Salvatore Alescio
'''

EXAMPLES = r'''
# Example Ansible Playbook
- name: create checkpoint of vm1
  hosts: windows
  tasks:
    - name: create snapshot
      hyperv_checkpoint:
        scvmm: "scvmm.contoso.com"
        username: "bill@contoso.com"
        password: "mypassword"
        vm: vm1
        state: create
        checkpoiint_name: "checkpoint with ansible"

'''

RETURN = '''
    description: Output of (Get-Host).Version powershell in JSON format
    returned: success
    type: string
'''

