#!powershell
# WANTS JSON
# POWERSHELL_COMMON



$params = Parse-Args $args

$scvmm = Get-Attr $params "scvmm" -failifempty $true
$username = Get-Attr $params "username"  -failifempty $true
$password = Get-Attr $params "password" -failifempty $true | ConvertTo-SecureString -AsPlainText -Force
$vm = Get-Attr $params "vm" -failifempty $true
$state = Get-Attr $params "state" -failifempty $true
$checkpoint_name = Get-Attr $params "checkpoint_name" -failifempty $false
$checkpoint_descr = Get-Attr $params "checkpoint_descr" -failifempty $false
$checkpoint_new_name = Get-Attr $params "checkpoint_new_name" -failifempty $false
$chechpoint_new_descr = Get-Attr $params "checkpoint_new_descr" -failifempty $false

Import-Module virtualmachinemanager

$cred = new-object -typename System.Management.Automation.PSCredential -argumentList $username, $password
$vmm = Get-SCVMMServer -ComputerName $scvmm -credential $cred


$result = New-Object psobject @{
    changed = $false
    status = ""
    task = ""
    vm_name = ""
    checkpoint_name = ""
    checkpoint_descr = ""
}



if($state -eq "create"){
  if($(Get-SCVirtualMachine -name $vm)){
    if((!$checkpoint_name) -and ($checkpoint_descr)){
        $checkpoint = New-SCVMCheckpoint -vm $vm -description $checkpoint_descr
        $result.checkpoint_descr = $checkpoint_descr
    }
    elseif (($checkpoint_name) -and (!$checkpoint_descr)){
        $checkpoint = New-SCVMCheckpoint -vm $vm -name $checkpoint_name
        $result.checkpoint_name = $checkpoint_name
    }
    elseif (($checkpoint_name) -and ($checkpoint_descr)){
        $checkpoint = New-SCVMCheckpoint -vm $vm -name $checkpoint_name -description $checkpoint_description
        $result.checkpoint_name = $checkpoint_name
        $result.checkpoint_descr = $checkpoint_descr
    }
    else{
      $checkpoint = New-SCVMCheckpoint -vm $vm
      $result.vm_name = $checkpoint.Name
    }

    if($checkpoint.MostRecentTask.Status -eq 5){
      $result.changed = $true
      $result.task = $checkpoint.MostRecentTask.Name
      $result.status = $checkpoint.MostRecentTask.StatusString
      $result.vm_name = $vm
    }
    else{
     Fail-Json "Something was wrong! Err: $checkpoint.MostRecentTask.StatusString"
    }
  }
  else{
    Fail-Json "$vm not exists! Please check VM name"
  }
}

elseif(($state -eq "remove") -or ($state -eq "restore")){
  $guest_vm = Get-SCVirtualMachine -name $vm
  if(!$guest_vm){
    Fail-Json "$vm not exists! Please check VM name"
  }
  elseif(!$checkpoint_name){
    Fail-Json "checkpoint name not esixts! Please enter  the checkpoint name"
  }
  else{
    foreach($check in $guest_vm.VMCheckpoints) {
      if($check.name -eq $checkpoint_name){
        if($state -eq "remove"){
          $checkpoint = Remove-SCVMCheckpoint -VMCheckpoint $check
        }
        if($state -eq "restore"){
          $checkpoint = Restore-SCVMCheckpoint -VMCheckpoint $check
        }
        if($checkpoint.MostRecentTask.Status -eq 5){
          $result.changed = $true
          $result.task = $checkpoint.MostRecentTask.Name
          $result.status = $checkpoint.MostRecentTask.StatusString
          $result.vm_name = $vm
          $result.checkpoint_name = $checkpoint_name
        }
        else{
          Fail-Json "Something was wrong! Err: $checkpoint.MostRecentTask.StatusString"
        }
      }
    }
  }
}

elseif($state -eq "remove_all"){
  $guest_vm = Get-SCVirtualMachine -name $vm
    if(!$guest_vm){
      Fail-Json "$vm not exists! Please check VM name"
    }
    else {
         if($guest_vm.VMCheckpoints.Length -ne 0){
          foreach($check in $guest_vm.VMCheckpoints){
            $checkpoint = Remove-SCVMCheckpoint -VMCheckpoint $check
            if($checkpoint.MostRecentTask.Status -ne 5){
              Fail-Json "SomeThing was wrong! Err: $checkpoint.MostRecentTask.StatusString"
            }
          }
          if($(Get-SCVMCheckpoint -VM $vm).Length -eq 0){
            $result.changed = $true
            $result.task = $checkpoint.MostRecentTask.Name
            $result.status = $checkpoint.MostRecentTask.StatusString
            $result.vm_name = $vm
            $result.checkpoint_name = $guest_vm.VMCheckpoint
          }
          else{
            Fail-Json "Something was wrong! All checkpoints are not removed!"
          }
         }

    }
}

elseif($state -eq "set"){
  $guest_vm = Get-SCVirtualMachine -name $vm
  if((!$checkpoint_new_name) -and (!$checkpoint_new_descr)){
    Fail-Json "value checkpoint_new_name or checkpoint_new_descr not exists! Please check the variables."
  }
  elseif(!$guest_vm){
    Fail-Json "$vm not exists! Please check VM name"
  }
  elseif(!$checkpoint_name){
    Fail-Json "checkpoint name not esixts! Please enter  the checkpoint name"
  }
  else{
    foreach($check in $guest_vm.VMCheckpoints){
      if($check.name -eq $checkpoint_name){
        if(($checkpoint_new_name) -and (!$checkpoint_new_descr)){
          $checkpoint = Set-SCVMCheckpoint -VMCheckpoint $check -Name $checkpoint_new_name
        }
        elseif((!$checkpoint_new_name) -and ($checkpoint_new_descr)){
          $checkpoint = Set-SCVMCheckpoint -VMCheckpoint $check -Description $checkpoint_new_descr
        }
        else{
         $checkpoint = Set-SCVMCheckpoint -VMCheckpoint $check -Name $checkpoint_new_name -Description $checkpoint_new_descr
        }
        if($checkpoint.MostRecentTask.Status -eq 5){
           $result.changed = $true
           $result.task = $checkpoint.MostRecentTask.Name
           $result.status = $checkpoint.MostRecentTask.StatusString
           $result.vm_name = $vm
           $result.checkpoint_name = $checkpoint_name
         }
        else{
          Fail-Json "Something was wrong! Err: $checkpoint.MostRecentTask.StatusString"
        }
      }
    }
  }
}

else{
  Fail-Json "$state not exists. Please select between 'create','remove', 'remove_all', 'set'"
}

Exit-Json $result

