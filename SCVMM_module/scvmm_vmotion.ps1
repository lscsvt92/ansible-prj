#!powershell
# WANTS JSON
# POWERSHELL_COMMON



$params = Parse-Args $args

$scvmm = Get-Attr $params "scvmm" -failifempty $true
$username = Get-Attr $params "username"  -failifempty $true
$password = Get-Attr $params "password" -failifempty $true | ConvertTo-SecureString -AsPlainText -Force
$vm = Get-Attr $params "vm" -failifempty $true
$destination_host = Get-Attr $params "destination_host" -failifempty $false
$destination_volume = Get-Attr $params "destination_volume" -failifempty $false

Import-Module virtualmachinemanager
#Import-Module FailoverClusters

$cred = new-object -typename System.Management.Automation.PSCredential -argumentList $username, $password
$vmm = Get-SCVMMServer -ComputerName $scvmm -credential $cred


$result = New-Object psobject @{
    changed = $false
    status = ""
    task = ""
    vm_name = ""
}



$guest_vm = Get-SCVirtualMachine -name $vm
$host_vm = Get-SCVMHost -ComputerName $guest_vm.VMHost
$clusterFQDN = $host_vm.HostCluster.Name
#$clus = $host.HostCluster.Name
if($guest_vm){
  $JobGroupID = [System.Guid]::NewGuid().ToString()
  if(($destination_host) -and (!$destination_volume)){
    $moveVM = Move-SCVirtualMachine -VMHost $destination_host -VM $vm -JobGroup $JobGroupID  -UseDiffDiskOptimization
    if($moveVM.MostRecentTask.Status -eq 5){
      $result.changed = $true
    }
  }
  elseif((!$destination_host) -and ($destination_volume)){
    #$csv_path = getCsvPath -CsvDest $destination_cluster_storage -FQDNcluster $clusterFQDN
    #$result.status = "sono qui"
    $cluster_r = Get-SCVMHostCluster
    $sharedVolume = $(Get-SCVMHostCluster -Name $clusterFQDN).SharedVolumes # ---> non riesce a lanciarlo!
    $result.status = "$sharedVolumes"
    foreach($csv in $cluster_r.SharedVolumes){
      #$result.status = "sono qui"
      #$csvInfo = $csv | select -Property Name -ExpandProperty SharedVolumeInfo
      if($csv.VolumeLabel -match $destination_volume){
        $csvPath = $csv.Name
      }
    }
    if($csvPath){
      #$result.status = "sono qui"
      $moveVM = Move-SCVirtualMachine -VMHost $guest.VMHost -Path $csvPath -JobGroup $JogGroupID -UseDiffDiskOptimization
    }
    if(moveVM.MostRecentTask.Status -eq 5){
      $result.changed = $true
    }
  }
  elseif($destination_host -and $destination_volume){
    $csv_path = getCsvPath $destination_cluster_storage $host.HostCluster.Name
     if($csv_path.getCsv){
       $moveVM = Move-SCVirtualMachine -VMHost $destination_cluster_storage -Path $csv_path.csvPath -JobGroup $JogGroupID -UseDiffDiskOptimization
     }
    $moveVM = Move-SCVirtualMachine -VMHost $destination_host -VM $vm -Path $destination_cluster_storage -JobGroup $JobGroupID -UseDiffDiskOptimization
    if($moveVM.MostRecentTask.Status -eq 5){
      $result.changed = $true
    }
  }
  else{
    Fail-Json "$vm not migrate! host and/or cluster storage destination not defined "
  }
  if($result.changed -eq $true){
        $result.status = $moveVM.MostRecentTask.StatusString
        $result.task = $moveVM.MostRecentTask.Name
        $result.vm_name = $vm
  }
}
else{
  Fail-Json "$vm not exists! Please check vm name!"
}


Exit-Json $result

